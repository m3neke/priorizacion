import { Link, withRouter } from "react-router-dom";
import React from "react";
import fakeAuth from '../../utils/fakeauth';
import '../../App.css';

const AuthButton = withRouter(
    ({ history }) =>
        fakeAuth.isAuthenticated ? (
            <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <img className="my-0 mr-md-auto" src="/logo_be.svg" alt="logo_be"/>
            <nav className="my-2 my-md-0 mr-md-3">
                 <Link className="p-2 text-dark" to="/encuesta">Encuesta</Link>
                 <Link className="p-2 text-dark" to="/iniciativas">Iniciativas</Link>
            </nav>
            <button className="btn btn-outline-primary"
                        onClick={() => {
                            fakeAuth.signout(() => history.push("/"));
                        }}
                    >
                        Salir
          </button>


            </div>

           
        ) : (
                <p></p>
            )
);

const Nav = () => {
    return (
        <AuthButton />
    )
}

export default Nav;