import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import {
    withRouter
} from 'react-router-dom';
import Composer from 'react-composer';
import './Encuesta.css';
import Loader from '../Loader/Loader';

const iniciativas = gql`
query {
    iniciativas {
      id
      name
      product_owner
    }
  }`;

const getSaldo = gql`
query getUser($id:ID){
    getUser(id:$id) {
      id
      name
      saldo
    }
  }
`;

const VOTACION_MUTATION = gql`
mutation addVotacion($votacion: [iniciativa]) {
    addVotacion(votacion:$votacion) {
      votacion
    }
  }
`;

const UPDATE_USER_MUTATION = gql`
mutation updateUser($id:ID) {
    updateUser(id:$id) {
      update
    }
  }
`;

let iniciativasAry = [];

class Encuestas extends Component {
    state = {
        saldo: 0,
        iniciativas: [],
        count: 0,
        loading: false
    }

    changeValue(value, index) {
        let saldoActual = 0;

        if (value) {
            iniciativasAry[index].saldo = parseInt(value);
            saldoActual = iniciativasAry.reduce((sumatoria, valorActual) => {
                return sumatoria + parseInt(valorActual.saldo);
            }, 0);
            this.setState({ saldo: saldoActual, iniciativas: iniciativasAry, count: 1 });
        } else {
            iniciativasAry[index].saldo = 0;
            saldoActual = iniciativasAry.reduce((sumatoria, valorActual) => {
                return sumatoria + parseInt(valorActual.saldo);
            }, 0);
            this.setState({ saldo: saldoActual, iniciativas: iniciativasAry, count: 1 });
        }
    }

    formatValue(value) {
        const separador = ".";
        const sepDecimal = ',';
        value += '';
        var splitStr = value.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
        }
        return `$${splitLeft + splitRight}`;
    }

    async saveVotation(mutationVotacion, mutationUpdateUser) {
        this.setState({ loading: true });
        const votacion = await mutationVotacion();
        await mutationUpdateUser();
        if (votacion.data.addVotacion.votacion) {
            alert('Gracias por votar');
            this.setState({ saldo: 0, iniciativas: [], count: 0, loading: false });
            iniciativasAry = [];
            this.props.history.push('/iniciativas');
        } else {
            alert('Ocurrió un error al votar');
            this.setState({ saldo: 0, iniciativas: [], count: 0, loading: false });
            iniciativasAry = [];
            this.props.history.push('/');
        }
    }

    render() {
        // if (this.state.count === 0) {
        return (
            <Query query={iniciativas} fetchPolicy='network-only' skip={this.state.count !== 0}>
                {({ loading: loadingI, data, error: errorIniciativa }) => (
                    <Query
                        query={getSaldo}
                        variables={{ id: localStorage.getItem('user') }}
                        fetchPolicy='network-only'>
                        {({ loading: loadingSaldo, data: { getUser }, error: errorSaldo }) => {
                            if (loadingSaldo || loadingI) {
                                return <Loader />;
                            }

                            if (data) {
                                data.iniciativas = data.iniciativas.map(i => {
                                    return {
                                        id: i.id,
                                        name: i.name,
                                        product_owner: i.product_owner,
                                        saldo: 0
                                    }
                                });
                                iniciativasAry = data.iniciativas;
                            }

                            let mensaje = '';
                            if (getUser.saldo - this.state.saldo < 0) {
                                mensaje = 'Has superado tu saldo';
                            } else {
                                mensaje = this.formatValue(getUser.saldo - this.state.saldo);
                            }

                            return (
                                //HTML
                                <div>
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-12">
                                                <ul className="list-group mb-3">
                                                    {iniciativasAry.map((branch, index) => (
                                                        <li key={branch.id} className="list-group-item d-flex justify-content-between lh-condensed">
                                                            <div className="col-8">
                                                                <label>{branch.name}</label>
                                                            </div>
                                                            <div className="col-4">
                                                                <input
                                                                    defaultValue={branch.saldo}
                                                                    onChange={e => { this.changeValue(e.target.value, index) }}
                                                                    onBlur={ e => { this.changeValue(e.target.value, index) }}
                                                                    type="number"
                                                                    min="0"
                                                                    placeholder="Ingresa tu inversión" className="form-control" />
                                                            </div>
                                                        </li>
                                                    ))}
                                                </ul>

                                            </div>
                                        </div>

                                    </div>
                                    <Composer
                                        components={[
                                            <Mutation
                                                mutation={VOTACION_MUTATION}
                                                variables={{ votacion: this.state.iniciativas }}
                                            />,
                                            <Mutation
                                                mutation={UPDATE_USER_MUTATION}
                                                variables={{ id: localStorage.getItem('user') }}
                                            />
                                        ]}
                                    >
                                        {
                                            ([VOTACION_MUTATION, UPDATE_USER_MUTATION]) => (
                                                <footer className="footer mt-auto py-3">
                                                    <div className="container">
                                                        <div className="row">
                                                            <div className="col-12 text-center b_b">
                                                                <h5 className="my-0 text-muted">{ this.state.saldo <= getUser.saldo ? 'Saldo' : '' }</h5>
                                                                <h4>{mensaje}</h4>
                                                            </div>
                                                            <div className="col-12">
                                                                <button className="btn btn-lg btn-primary btn-block btn-signin" disabled={(getUser.saldo - this.state.saldo) < 0 || this.state.saldo < getUser.saldo || this.state.loading} onClick={this.saveVotation.bind(this, VOTACION_MUTATION, UPDATE_USER_MUTATION)}>
                                                                    {!this.state.loading ? 'Guardar' : 'Guardando...'}
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </footer>
                                            )
                                        }
                                    </Composer>
                                </div>

                            )

                        }}
                    </Query>
                )}

            </Query>
        )
        // } 
        // else {
        //     return (
        //         <Query
        //             query={getSaldo}
        //             variables={{ id: localStorage.getItem('user') }}>
        //             {({ loading: loadingSaldo, data: { getUser }, error: errorSaldo }) => {
        //                 if (loadingSaldo) {
        //                     return <span>loading...</span>;
        //                 }
        //                 console.log('get Saldo 2 ', getUser.saldo);
        // let mensaje = '';
        // if (getUser.saldo - this.state.saldo < 0) {
        //     mensaje = 'Has superado tu saldo';
        // } else {
        //     mensaje = getUser.saldo - this.state.saldo;
        // }
        // return (
        //     //HTML
        //     <div>
        //         <div className="container">
        //             <div className="row">
        //                 <div className="col-12">
        //                     <ul className="list-group mb-3">
        //                         {iniciativasAry.map((branch, index) => (
        //                             <li key={branch.id} className="list-group-item d-flex justify-content-between lh-condensed">
        //                                 <div className="col-8">
        //                                     <label>{branch.name}</label>
        //                                 </div>
        //                                 <div className="col-4">
        //                                     <input
        //                                         defaultValue={branch.saldo}
        //                                         onBlur={e => { this.changeValue(e.target.value, index) }}
        //                                         type="number"
        //                                         min="0"
        //                                         placeholder="Ingresa tu inversión" className="form-control" />
        //                                 </div>
        //                             </li>
        //                         ))}
        //                     </ul>

        //                 </div>
        //             </div>

        //         </div>
        //         <Composer
        //             components={[
        //                 <Mutation
        //                     mutation={VOTACION_MUTATION}
        //                     variables={{ votacion: this.state.iniciativas }}
        //                 />,
        //                 <Mutation
        //                     mutation={UPDATE_USER_MUTATION}
        //                     variables={{ id: localStorage.getItem('user') }}
        //                 />
        //             ]}
        //         >
        //             {
        //                 ([VOTACION_MUTATION, UPDATE_USER_MUTATION]) => (
        //                     <footer className="footer mt-auto py-3">
        //                         <div className="container">
        //                             <div className="row">
        //                                 <div className="col-12 text-center b_b">
        //                                     <h5 className="my-0 text-muted">Saldo</h5>
        //                                     <h4>{mensaje}</h4>
        //                                 </div>
        //                                 <div className="col-12">
        //                                     <button className="btn btn-lg btn-primary btn-block btn-signin" disabled={(getUser.saldo - this.state.saldo) < 0 || this.state.saldo < getUser.saldo} onClick={this.saveVotation.bind(this, VOTACION_MUTATION, UPDATE_USER_MUTATION)}>Guardar</button>
        //                                 </div>
        //                             </div>
        //                         </div>
        //                     </footer>
        //                 )
        //             }
        //         </Composer>
        //     </div>

        // )

        //             }}
        //         </Query>
        //     )
        // }

    }
}


export default withRouter(Encuestas);
