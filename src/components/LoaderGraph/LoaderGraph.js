import React from 'react';
import './LoaderGraph.css';

const LoaderGraph = () => {
    return (
        <div className="loaderGraph-body">
            <div className="loaderGraph">
                <div className="loaderGraph__bar"></div>
                <div className="loaderGraph__bar"></div>
                <div className="loaderGraph__bar"></div>
                <div className="loaderGraph__bar"></div>
                <div className="loaderGraph__bar"></div>
                <div className="loaderGraph__ball"></div>
            </div>

        </div>

    )
}

export default LoaderGraph;