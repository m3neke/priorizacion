import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import { HorizontalBar } from 'react-chartjs-2';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import '../../App.css';
import Loader from '../Loader/Loader';
import LoaderGraph from '../LoaderGraph/LoaderGraph';

const iniciativas = gql`
query {
    votaciones {
      iniciativa
      totalAmount
      count
    }
  }
`;

const subscriptionData = gql`
subscription {
    votacionAdded {
      iniciativa,
      totalAmount,
      count
    }
  }
`;


class Iniciativas extends Component {

    render() {
        let unsubscribe = null;
        return (
            <Query query={iniciativas} fetchPolicy='network-only'>
                {({ loading, data, subscribeToMore, error }) => {
                    let labels = [];
                    let dataGraph = [];
                    let dataCount = [];
                    if (loading) {
                        return <Loader />;
                    }
                    if (!unsubscribe) {
                        unsubscribe = subscribeToMore({
                            document: subscriptionData,
                            updateQuery: (prev, data) => {
                                if (!data) return prev;
                                return {
                                    ...prev,
                                    votaciones: data.subscriptionData.data.votacionAdded
                                };
                            }
                        });
                    }
                    if (data.votaciones.length === 0) {
                        return (
                            <div className="container">
                                <div className="row">
                                    <div className="col-12">
                                        <h3 className="text-center">Esperando Votaciones</h3>
                                        <LoaderGraph />
                                    </div>
                                </div>
                            </div>
                        )
                    }

                    for (let i of data.votaciones) {
                        labels.push(i.iniciativa);
                        dataGraph.push(i.totalAmount);
                        dataCount.push(i.count);
                    }
                    //GRAFICO CIRCULAR
                    let queryData = {
                        labels,
                        datasets: [{
                            data: dataCount,
                            backgroundColor: [
                                '#FF6384',
                                '#36A2EB',
                                '#FFCE56'
                            ],
                            hoverBackgroundColor: [
                                '#FF6384',
                                '#36A2EB',
                                '#FFCE56'
                            ]
                        }]
                    };
                    //GRAFICO BARRA
                    let Hdata = {
                        labels: labels,
                        datasets: [
                            {
                                label: 'Iniciativas',
                                backgroundColor: [
                                    '#FF6384',
                                    '#36A2EB',
                                    '#FFCE56'
                                ],
                                borderColor: 'rgba(255,99,132,1)',
                                borderWidth: 1,
                                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                                hoverBorderColor: 'rgba(255,99,132,1)',
                                data: dataGraph
                            }
                        ]
                    };
                    return (
                        //HTML
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    < div className="card mb-4 shadow-sm" >
                                        <div className="card-header">
                                            <h4 className="my-0 font-weight-normal">Monto invertido por Iniciativa</h4>
                                        </div>
                                        <div className="card-body">
                                            <HorizontalBar data={Hdata} options={{
                                                scales: {
                                                    xAxes: [{
                                                        ticks: {
                                                            beginAtZero: true,
                                                            callback: (value) => {
                                                                const separador = ".";
                                                                const sepDecimal = ',';
                                                                value += '';
                                                                var splitStr = value.split('.');
                                                                var splitLeft = splitStr[0];
                                                                var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
                                                                var regx = /(\d+)(\d{3})/;
                                                                while (regx.test(splitLeft)) {
                                                                    splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
                                                                }
                                                                return `$${splitLeft + splitRight}`;
                                                            }
                                                        }
                                                    }]
                                                }
                                            }} />
                                        </div>
                                    </div >
                                </div >
                                <div className="col-12">
                                    < div className="card mb-4 shadow-sm" >
                                        <div className="card-header">
                                            <h4 className="my-0 font-weight-normal">Cantidad de Votaciones</h4>
                                        </div>
                                        <div className="card-body">
                                            <Doughnut data={queryData} />
                                        </div>
                                    </div >
                                </div>
                            </div>
                        </div>
                    )
                }}

            </Query>

        )
    }
}

export default Iniciativas

