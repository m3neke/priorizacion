import React, { Component } from 'react';
import fakeAuth from '../../utils/fakeauth';
import { Redirect } from "react-router-dom";
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import '../../App.css';
import './Login.css'


const LOGIN_MUTATION = gql`
    mutation login($user:String, $password:String) {
        login(user: $user, password:$password) {
            login
            id
        }
     }
`;

class Login extends Component {
    state = {
        redirectToReferrer: false,
        user: '',
        password: '',
        loading: false
    };

    login = (data) => {
        fakeAuth.authenticate(() => {
            if (data.login.login) {
                localStorage.setItem('user', data.login.id);
                this.setState({ redirectToReferrer: true, loading: false });
            } else {
                alert('contraseña o usuario incorrectos');
                this.setState({ redirectToReferrer: false, user: '', password: '', loading: false });
            }

        });
    };
    async mutation(fnc) {
        this.setState({ loading: true });
        const login = await fnc();
        this.login(login.data);
    }

    render() {
        let { from } = { from: { pathname: "/encuesta" } };
        let { redirectToReferrer } = this.state;
        const { user, password } = this.state
        if (redirectToReferrer) return <Redirect to={from} />;

        return (
             //HTML
            <div className="cardi card-container">
                <img className="logo-img" src="/logo.svg" alt="logo"/>
                <Mutation
                    mutation={LOGIN_MUTATION}
                    variables={{ user, password }}
                >
                    {mutation => (
                       
                        <div className="form-signin">
                            <input className="form-control"
                                value={user}
                                onChange={e => this.setState({ user: e.target.value.toLowerCase() })}
                                type="text"
                                placeholder="Usuario"
                            />

                            <input className="form-control"
                                value={password}
                                onChange={e => this.setState({ password: e.target.value })}
                                type="password"
                                placeholder="Contraseña"
                            />

                            <button className="btn btn-lg btn-primary btn-block btn-signin" disabled={this.state.loading} onClick={this.mutation.bind(this, mutation)}>
                                {!this.state.loading ? 'Login' : 'Ingresando...'}
                            </button>
                        </div>
                    )}
                </Mutation>
            </div>
        );
    }
}

export default Login;