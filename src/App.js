import React, { Component } from 'react';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import Iniciativas from './components/Iniciativas/Iniciativas';
import { BrowserRouter as Router, Redirect, Route } from "react-router-dom";
import fakeAuth from './utils/fakeauth';
import Nav from './components/Nav/Nav';
import Encuestas from './components/Encuesta/Encuesta';
import Login from './components/Login/Login';
import { getMainDefinition } from 'apollo-utilities';
import { ApolloLink, split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { InMemoryCache } from 'apollo-cache-inmemory';

// const client = new ApolloClient({
//   uri: "http://localhost:4000"
// });

const httpLink = new HttpLink({
  uri: 'https://votes-be.herokuapp.com/graphql',
  //uri: 'http://localhost:4000/graphql',
});

const wsLink = new WebSocketLink({
  uri: `wss://votes-be.herokuapp.com/graphql`,
  //uri: `ws://localhost:4000/graphql`,
  options: {
    reconnect: true,
  },
});

const terminatingLink = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return (
      kind === 'OperationDefinition' && operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const link = ApolloLink.from([terminatingLink]);

const cache = new InMemoryCache();

const client = new ApolloClient({
  link,
  cache
});

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        fakeAuth.isAuthenticated ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: props.location }
              }}
            />
          )
      }
    />
  );
}

class App extends Component {

  render() {
    return (
      <Router>
        <ApolloProvider client={client} >
          <Nav />
          <Route path="/" exact component={Login} />
          <PrivateRoute path="/iniciativas" component={Iniciativas} />
          <PrivateRoute path="/encuesta" component={Encuestas} />
        </ApolloProvider>
      </Router>

    );
  }
}

export default App;
